# Plant Leaf Disease Identification :shamrock:

A self-contained web page (Proposal/Report) for ECE 4554/5554 - Computer Vision Course project.

Authors: Ankit Parekh (ankitparekh@vt.edu) & Badhrinarayan Malolan (badhrinarayan@vt.edu) [Canvas Group: Computer Vision Project 49]

We have used template from [here](https://cs.stanford.edu/people/karpathy/deepimagesent/) as suggested in the project guidelines.

## Repository Structure

| File/Folder          | _Description_                                                                                                                                        |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| **index.html**       | _HTML page for Project Proposal/Report_                                                                                                              |
| **webpage_images**   | _Folder containing images used in Project Report_                                                                                                    |
| **.gitlab-ci.yml**   | _CI file to host page on Gitlab Pages_                                                                                                               |
| **project_material** | _Folder containing Project code/notebooks, HTML versions of notebooks, model weights, model & training history pickle dumps, and other output files_ |
| **README.md**        | _Description file for the repository_                                                                                                                |

## project_material Folder

:file_folder: **binary_classification_for_specific_leaf_type_using_ml** : Folder for the task of binary classification of a specific leaf type ("Apple") using ML model
*  featureextraction_plant_disease_identification_eligibletype.ipynb   : IPYNB Notebook
*  featureextraction_plant_disease_identification_eligibletype.html : HTML version of notebook
*  xgboost_imsize=500x500_perclsimgs=1645_clsnum=2_leaftype=Apple_tstacc=0.98.pkl : Model .pkl file
*  globalfeatures_imsize=500x500_perclsimgs=1645_clsnum=2_leaftype=Apple.pkl : Global features .pkl file
*  labels_imsize=500x500_perclsimgs=1645_clsnum=2_leaftype=Apple.pkl : Labels .pkl file

:file_folder: **binary_classification_using_ml** : Folder for the task of binary classification for whole dataset using ML model
*  featureextraction_plant_disease_identification_binary_classification.ipynb   : IPYNB Notebook
*  featureextraction_plant_disease_identification_binary_classification.html : HTML version of notebook
*  xgboost_imsize=500x500_perclsimgs=10000_clsnum=2_tstacc=0.95.pkl : Model .pkl file
*  globalfeatures_imsize=500x500_perclsimgs=10000_clsnum=2.pkl : Global features .pkl file
*  labels_imsize=500x500_perclsimgs=10000_clsnum=2.pkl : Labels .pkl file

:file_folder: **multiclass_classification_using_ml** : Folder for the task of multiclass classification for whole dataset using ML model
*  featureextraction_plant_disease_identification_multiclass_classification.ipynb   : IPYNB Notebook
*  featureextraction_plant_disease_identification_multiclass_classification.html : HTML version of notebook
*  xgboost_imsize=500x500_perclsimgs=800_clsnum=38_tstacc=0.89.pkl : Model .pkl file
*  globalfeatures_imsize=500x500_perclsimgs=800_clsnum=38.pkl : Global features .pkl file
*  labels_imsize=500x500_perclsimgs=800_clsnum=38.pkl : Labels .pkl file

:file_folder: **binary_classification_for_specific_leaf_type_using_cnn** : Folder for the task of binary classification of a specific leaf type ("Apple") using CNN model
*  cnn_plant_disease_identification_binary_classification_specific_leaf_type.ipynb   : IPYNB Notebook
*  cnn_plant_disease_identification_binary_classification_specific_leaf_type(I).html : HTML version of notebook
*  cnn_epochs 24_acc 0.9763.h5 : Best model weights H5 file
*  HistoryDict.pickle : Training history .pkl file
*  accuracy.png : Train & Val accuracy plot for model training

:file_folder: **binary_classification_using_cnn** : Folder for the task of binary classification for whole dataset using CNN model
*  cnn_plant_disease_identification_binary_classification.ipynb   : IPYNB Notebook
*  cnn_plant_disease_identification_binary_classification.html : HTML version of notebook
*  cnn_epochs 05_acc 0.9857.h5 : Best model weights H5 file
*  HistoryDict.pickle : Training history .pkl file
*  accuracy.png : Train & Val accuracy plot for model training
   
:file_folder: **multiclass_classification_using_cnn** : Folder for the task of multiclass classification for whole dataset using CNN model
*  cnn_plant_disease_identification_multiclass_classification.ipynb   : IPYNB Notebook
*  cnn_plant_disease_identification_multiclass_classification.html : HTML version of notebook
*  cnn_epochs 30_acc 0.9738.h5 : Best model weights H5 file
*  HistoryDict.pickle : Training history .pkl file
*  accuracy.png : Train & Val accuracy plot for model training

:open_file_folder: **cnn_qualitative_results.ipynb & cnn_qualitative_results.html**   : IPYNB Notebook & HTML version of notebook for Qualitative Results of CNN models :bookmark:

:open_file_folder: **ml_qualitative_results.ipynb & ml_qualitative_results.html**     : IPYNB Notebook & HTML version of notebook for Qualitative Results of ML models :bookmark:

:open_file_folder: **ml_model_experimentation.ipynb & ml_model_experimentation.html** : IPYNB Notebook & HTML version of notebook for feature selection and ML model experimentation process. We experiment with multiple features and ML models

## List of features and models experimented within ml_model_experimentation.ipynb

### Features extracted for feature selection:
1.  First Order Statistics/Statistical Features (FOS/SF)
2.  Gray Level Co-occurence Matrix (GLCM/SGLDM)
3.  Gray Level Difference Statistics (GLDS)
4.  Neighborhood Gray Tone Difference Matrix (NGTDM)
5.  Statistical Feature Matrix (SFM)
6.  Law's Texture Energy Measures (LTE/TEM)
7.  Fractal Dimension Texture Analysis (FDTA)
8.  Gray Level Run Length Matrix (GLRLM)
9.  Gray Level Size Zone Matrix (GLSZM)
10. Higher Order Spectra (HOS)
11. Local Binary Patterns (LPB)
12. Gray-scale Morphological Analysis
13. Multilevel Binary Morphological Analysis
14. Gabor Transform (GT)
15. Threshold Adjacency Matrix (TAS)
16. Histogram of Oriented Gradients (HOG)
17. Hu moments
18. Zernike Moments :white_check_mark:
19. Haralick Features :white_check_mark:
20. Color Histogram :white_check_mark:

### ML models trained for model selection:
1. Logistic Regression
2. Linear Discriminant Analysis
3. K Nearest Neighbors
4. Classification And Regression Tree (CART)
5. Random Forest
6. Naive Bayes
7. XGBoost :white_check_mark:
8. Support Vector Machine
